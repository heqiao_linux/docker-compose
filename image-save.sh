#!/bin/bash
# 镜像名称
TAG="ssh-22.04"
REPOSITORY_TAG="ubuntu-22.04-ssh:latest"
#构建镜像cf
docker build -f $(pwd)/Dockerfile --progress=plain -t  $REPOSITORY_TAG $(pwd)/
# 判断构建镜像是否成功 如果失败就退出
if [ $? -ne 0 ];then
    echo "镜像构建失败"
    exit 1
fi

#  docker 通过镜像 REPOSITORY TAG 获取id
IMAGEID=$(docker images --format "{{.ID}}\t{{.Repository}}:{{.Tag}}" | grep "${REPOSITORY_TAG}" | awk '{print $1}')
# 镜像 地址
IMAGEUIR="crpi-vb403qyxdijc0eq9.cn-guangzhou.personal.cr.aliyuncs.com/utlis/ubuntu:$TAG"
# 登录阿里云docker 服务
echo "qq..1813033378" | docker login --username=qq1813033378 --password-stdin crpi-vb403qyxdijc0eq9.cn-guangzhou.personal.cr.aliyuncs.com
# 标签镜像
docker tag $IMAGEID $IMAGEUIR
docker push $IMAGEUIR
# 判断推送镜像是否成功
if [ $? -ne 0 ];then
    echo "镜像推送失败"
    exit 1
fi
echo "镜像推送成功"

