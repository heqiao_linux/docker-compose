#!/bin/bash
# 获取脚本所在的目录路径
DOCKER_FFMPEG_BUILD_DIR=$(dirname $(readlink -f "$0"))

# ssh 数据挂载目录
export WORK_DIR=/mnt/nas/work
# 判断 mysql 目录是否存在如果不存在这创建
if [ ! -d "${WORK_DIR}" ]; then
  mkdir -p ${WORK_DIR}
  chmod -R 777 ${WORK_DIR}
fi

# ssh 容器名称
export SSH_VESSEL_NAME="ssh"
# 判断 ssh 容器是否存在如果存在就删除
if [ "$(docker ps -a | grep "${SSH_VESSEL_NAME}")" ]; then
  docker rm -f $(docker ps -a | grep "${SSH_VESSEL_NAME}" | awk '{print $1}')
fi
# 镜像名称
TAG="ssh-22.04"
# 镜像 地址
export IMAGEUIR="crpi-vb403qyxdijc0eq9.cn-guangzhou.personal.cr.aliyuncs.com/utlis/build:$TAG"

# 启动容器
docker-compose -f ./run-aliyunc.yml up -d






