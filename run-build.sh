#!/bin/bash
# ssh 数据挂载目录
export WORK_DIR=/mnt/nas/work
# 判断 mysql 目录是否存在如果不存在这创建
if [ ! -d "${WORK_DIR}" ]; then
  mkdir -p ${WORK_DIR}
fi
chmod -R 777 ${WORK_DIR}
# 容器名称
export VESSEL_NAME="ssh"

# 判断 ssh 容器是否存在如果存在就删除
if [ "$(docker ps -a | grep "${TAG}")" ]; then
  docker rm -f $(docker ps -a | grep "${TAG}" | awk '{print $1}')
fi
# 镜像 地址
export IMAGEUIR="ubuntu-22-04:ssh"

# 启动容器
docker-compose -f ./build-ssh.yml up --build -d


