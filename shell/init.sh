#!/bin/bash
# 初始化脚本
apt-get update -y
apt-get install -y openssh-server
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
sed -i 's/#UsePAM no/UsePAM no/' /etc/ssh/sshd_config
sed -i 's/#Port 22/Port 22/' /etc/ssh/sshd_config
# 设置 root 密码
echo "root:${ROOT_PASSWORD}" | chpasswd
# 更换阿里镜像原
cat ./ssh-key/sources.list > /etc/apt/sources.list
ssh-keygen -t rsa -b 2048 -C "1813033378@qq.com" -N '' -f /root/.ssh/id_rsa
cat ./ssh-key/ubuntu-22-04.pub > /root/.ssh/authorized_keys
cat ./ssh-key/git > /root/.ssh/git
cat ./ssh-key/nas > /root/.ssh/nas
cat ./ssh-key/config > /root/.ssh/config
cp ./start.sh /start.sh
chmod +x /start.sh
chmod 600 /root/.ssh/authorized_keys /root/.ssh/id_rsa /root/.ssh/id_rsa.pub /root/.ssh/git /root/.ssh/nas
rm -rf ./*
apt-get clean
rm -rf /var/lib/apt/lists/* /var/lib/apt/archives/* /tmp/*











