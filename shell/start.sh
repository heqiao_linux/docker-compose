#!/bin/bash
# 判断目录是否存在如果不存在这创建
SSHDIR="/run/sshd"
if [ ! -d "${SSHDIR}" ]; then
  mkdir -p $SSHDIR
  chown root:root $SSHDIR
  chmod 755 $SSHDIR
fi
# 设置 root 密码 a user with a random password
echo "root:${ROOT_PASSWORD}" | chpasswd
# 启动服务
/usr/sbin/sshd -D


