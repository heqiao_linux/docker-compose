# 编译环境镜像
FROM ubuntu:22.04
 # 作者
LABEL u="何桥"
COPY ./shell /tmp/shell
#配置 root 服务密钥
ENV ROOT_PASSWORD 123456
# 初始化脚本
RUN cd /tmp/shell && chmod +x ./*.sh && ./init.sh
# 指定容器监听的端口
EXPOSE 22
# 启动脚本
ENTRYPOINT ["/start.sh"]
